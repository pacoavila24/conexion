
package calculadora;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
/**
 *
 * @author Avila
 */
public class Pruebas {
    @Test
	public void testSuma(){
		Calculadora calc = new Calculadora();
		double resultado = calc.sumar(1,1);
		assertEquals(2,resultado,0);
	}
        
        @Test
	public void testResta(){
		Calculadora calc = new Calculadora();
		double resultado = calc.restar(1,1);
		assertEquals(0,resultado,0);

	}
        
        @Test
	public void testMultiplicacion(){
		Calculadora calc = new Calculadora();
		double resultado = calc.multiplicar(2,2);
		assertEquals(4,resultado,0);

	}
        
        @Test
	public void testDivision(){
		Calculadora calc = new Calculadora();
		double resultado = calc.dividir(6,2);
		assertEquals(3,resultado,0);
	}
        
                @Test
	public void testSeno(){
		Calculadora calc = new Calculadora();
                double resultado = calc.seno(90);
		assertEquals(0.850903525,resultado,0);
	}

        @Test
	public void testCoseno(){
		Calculadora calc = new Calculadora();
                double resultado = calc.coseno(0);
		assertEquals(1,resultado,0);
	}
        
        @Test
	public void testTangente(){
		Calculadora calc = new Calculadora();
                double resultado = calc.tangente(1);
		assertEquals(1.557407725,resultado,0);
	}
}
