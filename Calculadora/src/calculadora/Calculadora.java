/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calculadora;

/**
 *
 * @author Avila
 */
public class Calculadora {


   public double sumar(double num1, double num2) {
       double resultado = num1 + num2;
       return resultado;
   }
   
   public double restar(double num1, double num2) {
       double resultado = num1 - num2;
       return resultado;
   }
   
   public double multiplicar(double num1, double num2) {
       double resultado = num1 * num2;
       return resultado;
   }
   
   public double dividir(double num1, double num2) {
       double resultado = num1 / num2;
       return resultado;
   }
   
   public static double seno(double num){
        double resultado;
        resultado = Math.sin(Math.toRadians(num));
        return resultado;
    }
    
    public static double coseno(double num){
        double resultado;
        resultado = Math.cos(Math.toRadians(num));
        return resultado;
    }
    
    public static double tangente(double num){
        double resultado;
        resultado = Math.tan(Math.toRadians(num));
        return resultado;
    }

    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
